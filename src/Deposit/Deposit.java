package Deposit;

import org.w3c.dom.Element;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Deposit implements Comparable<Deposit> {


    private String customerNumber;
    private BigDecimal amount;
    private int duration;
    private DepositType depositType;
    private BigDecimal depositInterest;
    private String depositTypeValue;

    public String getDepositTypeValue() {
        return depositTypeValue;
    }


    public BigDecimal getDepositInterest() {
        return depositInterest;
    }

    public void setDepositInterest(BigDecimal depositInterest) {
        this.depositInterest = depositInterest;
    }


    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public Deposit() { }


    public void setDeposit(String customerNumber, BigDecimal amount, int duration, DepositType depositType) {
        this.customerNumber = customerNumber;
        this.amount = amount;
        this.duration = duration;
        this.depositType = depositType;
    }

    public void calculateDepositInterest() {
        BigDecimal rate = new BigDecimal(depositType.getRate());
        BigDecimal duration = new BigDecimal(getDuration());
        setDepositInterest(amount.multiply(duration).multiply(rate).divide(new BigDecimal(36500), RoundingMode.HALF_UP));
    }

    public void depositParameter(Element eElement) {
        try {
            this.amount = BigDecimal.valueOf(Long.parseLong(eElement
                    .getElementsByTagName("depositBalance")
                    .item(0)
                    .getTextContent()));
            this.duration = Integer.parseInt(eElement
                    .getElementsByTagName("durationInDays")
                    .item(0)
                    .getTextContent());
            this.customerNumber = String.valueOf(eElement
                    .getElementsByTagName("customerNumber")
                    .item(0)
                    .getTextContent());
            this.depositTypeValue = String.valueOf(eElement
                    .getElementsByTagName("depositType")
                    .item(0)
                    .getTextContent());
        } catch (NumberFormatException e) {
            System.out.println(e);
        }


    }


    @Override
    public int compareTo(Deposit o) {
        return this.depositInterest.compareTo(o.depositInterest);
    }
}
