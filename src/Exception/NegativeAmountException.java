package Exception;

public class NegativeAmountException extends BusinessException{

    public NegativeAmountException(String message) {
        super(message);
    }
}

