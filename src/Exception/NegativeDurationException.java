package Exception;

public class NegativeDurationException extends BusinessException{
    public NegativeDurationException(String message) {
        super(message);
    }
}
