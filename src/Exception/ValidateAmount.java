package Exception;

import java.math.BigDecimal;

public class ValidateAmount {

    public ValidateAmount(BigDecimal amount) throws NegativeAmountException {

        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new NegativeAmountException("Negative Amount " + amount);
        }
    }
}
