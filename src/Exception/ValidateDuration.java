package Exception;

public class ValidateDuration {
    public ValidateDuration(Integer duration) throws NegativeDurationException {
        if (duration < 0) {
            throw new NegativeDurationException("Negative duration " + duration);
        }
    }
}
