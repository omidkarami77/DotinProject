import Deposit.Deposit;
import File.ReadFile;
import File.WriteFile;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.math.BigDecimal;
import java.util.ArrayList;
import Exception.ValidateAmount;
import Exception.ValidateDuration;
import Exception.BusinessException;
import Deposit.DepositType;

public class main {
    public static void main(String[] args) {
        ArrayList<Deposit> depositArrayList = new ArrayList<>();
        String path = "src/file.txt";
        ReadFile readFile = new ReadFile(path);
        try {
            NodeList nList = readFile.getnList();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    try {
                        Deposit depositStart = new Deposit();
                        depositStart.depositParameter(eElement);
                        BigDecimal amount = depositStart.getAmount();
                        Integer duration = depositStart.getDuration();
                        new ValidateAmount(amount);
                        new ValidateDuration(duration);
                        String customerNumber = depositStart.getCustomerNumber();
                        String depositTypeValue = depositStart.getDepositTypeValue();
                        System.out.println(customerNumber + " " + amount + " " + duration);
                        DepositType depositType = (DepositType) Class.forName(depositTypeValue).newInstance();
                        depositType.setRate();
                        depositStart.setDeposit(customerNumber,amount,duration,depositType);
                        depositStart.calculateDepositInterest();
                        depositArrayList.add(depositStart);
                    } catch (ClassNotFoundException | BusinessException | NullPointerException e) {
                        System.out.println(e);
                    }
                }
            }
            new WriteFile(depositArrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
